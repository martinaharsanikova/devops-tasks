# Zadání pro úlohu Docker

Zkuste si přiloženou aplikaci nejprve zprovoznit bez použití Dockeru, pomocí níže uvedeného postupu.

Až ověříte funkčnost aplikace, vytvořte soubor `Dockerfile` pro sestavení Docker image této aplikace.

Následně zkuste aplikaci spustit v kontejneru, s využitím sestavené Docker image.

## Postup pro zprovoznění aplikace bez použití Dockeru

1. Ověřte, že máte na cílovém serveru nainstalovaný Python 3.8 nebo novější
   - _Tip: aktuální verzi Python3 zjistíte pomocí `python3 -V`_
2. Nainstalujte závislosti: `pip3 install -r requirements.txt`
   - _Poznámka: My zde pro zjednodušení instalujeme závislosti globálně. V běžné praxi se ale pro instalaci Python závislostí většinou používá virtuální prostředí (virtualenv)._
3. Přejděte do projektového podadresáře: `cd clinic`
4. Spusťte DB migraci: `python3 manage.py migrate`
5. Spusťte server: `gunicorn clinic.wsgi`

## Ověření základní funkčnosti aplikace (smoke test)

1. Po spuštění serveru otevřete v prohlížeči adresu http://localhost:9000.
   - Zobrazí se vám stránka aplikace?
2. V aplikaci otevřete záložku `Naši veterináři`.
   - Zobrazí se seznam veterinářů nebo databázová chyba?
   - Pokud vidíte DB chybu, zkontrolujte, zda jste nezapomněli spustit DB migraci.
3. V aplikaci otevřete záložku **Sjednat schůzku**, vyplňte a odešlete formulář.
   - Pokud odeslání skončí úspěšně, lze aplikaci považovat za funkční

## Nápověda

Níže najdete rámcový postup řešení úlohy:

1. Začněte vytvořením prázdného souboru `Dockerfile` v kořenovém adresáři projektu.
2. Uvnitř souboru zvolte jako první správnou _base image_, která obsahuje Python v potřebné verzi.
   - _Tip: Raději než `-alpine` zkuste `-slim` nebo variantu bez přívlastku. Alpine totiž nemá v základu všechny knihovny, které jsou potřeba - projeví se to chybou při volání `pip install`._
3. Poté přidejte instrukce pro instalaci závislostí a přidejte i samotnou aplikaci.
4. Nakonec nastavte výchozí příkaz pro spuštění
5. Po sestavení Docker image vytvořte a spusťte kontejner
6. Uvnitř kontejneru spusťte příkaz pro DB migraci
7. Oveřte funkčnost aplikace

## Bonus: Entrypoint

Nastavením [entrypointu](https://docs.docker.com/engine/reference/builder/#entrypoint) můžete zajistit automatické spuštění databázových migrací. Nebudete tedy muset ručně volat příkaz `python manage.py migrate` – aplikace si ho zavolá sama při každém spuštění.

## Řešení

Řešením úlohy je soubor `Dockerfile`, kterým lze sestavit Docker image aplikace.

Výsledné řešení najdete v souboru `reseni-docker.b64`. Je zakódované pomocí `base64`, aby nebylo na první pohled čitelné.

Než nahlédnete do řešení, zkuste úlohu vyřešit s pomocí nápověd uvedených výše.

Pokud opravdu chcete zobrazit řešení, zavolejte `base64 -d reseni-docker.b64`.
