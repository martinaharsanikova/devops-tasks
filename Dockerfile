# Use the official Python 3.8 image
FROM python:3.8

# Set the working directory
WORKDIR /app

# Copy the project files into the image
COPY . /app

# Install dependencies
RUN pip install -r requirements.txt

COPY clinic .

# DB migrace
RUN python manage.py migrate

# Start the Gunicorn server
CMD ["gunicorn", "clinic.wsgi:application", "--bind", "0.0.0.0:9000"]



